/*
Copyright (c) 2015 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

(function (document) {
  'use strict';

  // Grab a reference to our auto-binding template
  // and give it some initial binding values
  // Learn more about auto-binding templates at http://goo.gl/Dx1u2g
  var app = document.querySelector('#app');

  // Sets app default base URL
  app.baseUrl = '/';
  if (window.location.port === '') { // if production
    // Uncomment app.baseURL below and
    // set app.baseURL to '/your-pathname/' if running from folder in production
    // app.baseUrl = '/polymer-starter-kit/';
  }

  app.displayInstalledToast = function () {
    // Check to make sure caching is actually enabled—it won't be in the dev environment.
    if (!Polymer.dom(document).querySelector('platinum-sw-cache').disabled) {
      Polymer.dom(document).querySelector('#caching-complete').show();
    }
  };

  // Listen for template bound event to know when bindings
  // have resolved and content has been stamped to the page
  app.addEventListener('dom-change', function () {
    console.log('Ready...');
    console.log('Steady...');
    console.log('GO !');
  });

  // See https://github.com/Polymer/polymer/issues/1381
  window.addEventListener('WebComponentsReady', function () {
    app.setupDrawer();
    app.$.customLayout.$.homeTab.click();
  });

  app.setupDrawer = function () {
    app.$.customLayout.$.tabs.setAttribute('style', 'width: ' + (app.$.customLayout.$.drawerPanel.clientHeight - app.$.customLayout.$.toolbar.clientHeight) + 'px;');
    app.transform('rotate(-90deg) translateY(-' + (app.$.customLayout.$.drawerPanel.clientHeight - app.$.customLayout.$.toolbar.clientHeight) + 'px)', app.$.customLayout.$.tabs);
  };

  // Scroll page to top and expand header
  app.scrollPageToTop = function () {
    //app.$.customLayout.$.headerPanelMain.scrollToTop(true);
  };

  app.closeDrawer = function () {
    app.$.customLayout.$.drawerPanel.closeDrawer();
  };

  app.sendMail = function (email, name, sub, body) {
    window.open("http://fliife.xyz/mail.php?email=" + email + "&name=" + name + "&sub=" + sub + "&body=" + body);
    app.$.toast.text = 'Thanks for sending an email !';
    app.$.toast.show();
  };



  app.setColor = function (color) {
      //Absolutely don't know why, but this has to be run four times. Any clue ?
    for (var threeTimes = 0; threeTimes < 4; threeTimes++) {
        //Here, 3 is the number of colours. You can change this to match your needs.
      for (var k = 0; k < 3; k++) {
        if (k !== color) {
          var bgOld = document.getElementsByClassName('bg' + k);
          //Get the current class list, change the bg[0-9] class to `'bg' + @color`, and set it as new class list.
          for (var i = 0; i < bgOld.length; i++) {
            var elem = bgOld[i];
            var currentClass = elem.getAttribute('class');
            var newClass = currentClass.split('bg' + k).join('bg' + color);
            elem.setAttribute('class', newClass);
          }
        }
      }
    }
  };

})(document);
